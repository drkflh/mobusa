<?php

use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DataPemesananController;
use App\Http\Controllers\DashboardController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/form-pemesanan', function () {
    return view('userpanel.formpemesanan');
});


Route::post('/datapemesanan', [DataPemesananController::class, 'store'])->name('datapemesanan.store');

Route::get('/form-pembatalan', [DataPemesananController::class, 'showSearchForm'])->name('datapembataln.get');
Route::post('/form-pembatalan', [DataPemesananController::class, 'searchForCancellation'])->name('search.pembatalan');
Route::post('/cancel/{id}', [DataPemesananController::class, 'cancelOrder'])->name('order.cancel');

Route::middleware('auth')->group(function () {
    Route::get('/dashboardadmin', [DashboardController::class, 'index'])->name('dashboard');

    Route::get('/datapemesanan', [DataPemesananController::class, 'index'])->name('admin.datapemesanan.index');
    Route::put('/datapemesanan/{id}/update-status', [DataPemesananController::class, 'updateStatus'])->name('admin.datapemesanan.update_status');

    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

require __DIR__.'/auth.php';
