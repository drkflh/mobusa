@extends('userpanel.layouts.guest')

@section('title', "Beranda")

@section('content')
@include('userpanel.partials.slider')
@include('userpanel.partials.about')
@include('userpanel.partials.gallery')
@include('userpanel.partials.product')
@include('userpanel.partials.toko')
@endsection
