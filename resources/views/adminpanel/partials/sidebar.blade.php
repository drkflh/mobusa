<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="sidebar-brand-text mx-3">
            <img src="{{ asset('adminpanel/img/LogoMobusa.png') }}" alt="Logo" height="30">
        </div>
        
    </a>

    <hr class="sidebar-divider my-0">

    <li class="nav-item {{ request()->is('dashboardadmin') ? 'active' : '' }}">
        <a class="nav-link" href="/dashboardadmin">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span></a>
    </li>

    <hr class="sidebar-divider">

    <li class="nav-item {{ request()->is('datapemesanan') ? 'active' : '' }}">
        <a class="nav-link" href="/datapemesanan">
            <i class="fas fa-fw fa-table"></i>
            <span>Data Pemesanan</span></a>
    </li>

    <hr class="sidebar-divider d-none d-md-block">

    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>