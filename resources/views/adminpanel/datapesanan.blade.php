@extends('adminpanel.layouts.app')

@section('title', "Data Pesanan")

@section('content')

<div class="container-fluid">
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Data Pesanan</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>No HP</th>
                            <th>Lokasi COD</th>
                            <th>Jam COD</th>
                            <th>Tanggal Pesanan</th>
                            <th>Product Pemesan</th>
                            <th>Aroma</th>
                            <th>Jumlah Product</th>
                            <th>Total Harga</th>
                            <th>Alasan Cancel</th>
                            <th>Status Transaksi</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>No HP</th>
                            <th>Lokasi COD</th>
                            <th>Jam COD</th>
                            <th>Tanggal Pesanan</th>
                            <th>Product Pemesan</th>
                            <th>Aroma</th>
                            <th>Jumlah Product</th>
                            <th>Total Harga</th>
                            <th>Alasan Cancel</th>
                            <th>Status Transaksi</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        @foreach ($dataPemesanans as $key => $dataPemesanan)
                            <tr>
                                <td>{{ $key + 1 }}</td>
                                <td>{{ $dataPemesanan->nama }}</td>
                                <td>
                                    <a href="https://wa.me/62{{ ltrim($dataPemesanan->no_hp, '0') }}" target="_blank">
                                        {{ $dataPemesanan->no_hp }}
                                    </a>                                    
                                </td>
                                <td>{{ $dataPemesanan->lokasi_cod }}</td>
                                <td>{{ $dataPemesanan->time }}</td> 
                                <td>{{ $dataPemesanan->date }}</td>
                                <td>{{ $dataPemesanan->product_pemesan }}</td>
                                <td>{{ $dataPemesanan->aroma }}</td>
                                <td>{{ $dataPemesanan->jumlah_product }}</td>
                                <td>{{ 'Rp ' . number_format($dataPemesanan->total_harga, 0, ',', '.') }}</td>
                                <td>{{ $dataPemesanan->alasan_cancel }}</td>
                                <td>
                                    @if ($dataPemesanan->status_transaksi == 'proses')
                                        <form id="form-update-status-{{ $dataPemesanan->id }}" action="{{ route('admin.datapemesanan.update_status', $dataPemesanan->id) }}" method="POST">
                                            @csrf
                                            @method('PUT')
                                            <button type="button" class="btn btn-warning btn-icon-split" onclick="confirmUpdateStatus({{ $dataPemesanan->id }})">
                                                <span class="icon text-white-50">
                                                    <i class="fas fa-exclamation-triangle"></i>
                                                </span>
                                                <span class="text">{{ $dataPemesanan->status_transaksi }}</span>
                                            </button>
                                        </form>
                                    @elseif ($dataPemesanan->status_transaksi == 'selesai')
                                        <button class="btn btn-success btn-icon-split" disabled>
                                            <span class="icon text-white-50">
                                                <i class="fas fa-check"></i>
                                            </span>
                                            <span class="text">{{ $dataPemesanan->status_transaksi }}</span>
                                        </button>
                                    @elseif ($dataPemesanan->status_transaksi == 'cancel')
                                        <button class="btn btn-danger btn-icon-split" disabled>
                                            <span class="icon text-white-50">
                                                <i class="fas fa-ban"></i>
                                            </span>
                                            <span class="text">{{ $dataPemesanan->status_transaksi }}</span>
                                        </button>
                                    @endif
                                </td>
                                
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
    function confirmUpdateStatus(id) {
        if (confirm('Apakah Anda yakin ingin menyelesaikan pesanan ini?')) {
            document.getElementById('form-update-status-' + id).submit();
        }
    }
</script>

@endsection