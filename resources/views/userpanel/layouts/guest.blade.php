<!DOCTYPE html>
<html lang="id">
<head>
	<meta charset="UTF-8">
	<title>Mobusa Shamppo Premium</title>

	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="drkflh">

    <meta http-equiv="content-language" content="id-id">
    <meta property="og:url" content="https://mobusa.my.id" />
    <meta property="og:title" content="Mobusa Shamppo Premium">
	<meta name="description" content="Mobusa Shampoo Premium: Formula khusus untuk mencuci motor dan mobil Anda. Menghilangkan kotoran dan noda dengan mudah, memberikan kilap ekstra, dan melindungi cat kendaraan Anda. Temukan produk pembersih terbaik untuk kendaraan kesayangan Anda dengan Mobusa Shampoo Premium." />
    <meta property="og:locale" content="id_ID" />
	<meta property="og:type" content="website" />
    <meta name="keyword" content="Mobusa Shamppo Premium, Shampoo Motor, Shamppo Mobil, Sabun Motor, Sabun Mobil ">
    <meta property="og:site_name" content="Mobusa Shamppo Premium" />

	<link rel="stylesheet" href="{{asset('userpanel/css/style.css')}}">
	<link rel="stylesheet" href="{{asset('userpanel/css/responsive.css')}}">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@11/dist/sweetalert2.min.css">
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11/dist/sweetalert2.all.min.js"></script>

    <link rel="apple-touch-icon" sizes="180x180" href="images/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" href="images/favicon/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="images/favicon/favicon-16x16.png" sizes="16x16">

</head>
<body>
<div class="boxed_wrapper">
@include('userpanel.partials.header')
@yield('content')
@include('userpanel.partials.copyright')
@include('userpanel.partials.footer')
  

</div>

<div class="scroll-to-top scroll-to-target" data-target="html"><span class="fa fa-chevron-circle-up"></span></div>

<script src="{{asset('userpanel/js/jquery-1.11.1.min.js')}}"></script>
<script src="{{asset('userpanel/js/wow.js')}}"></script>
<script src="{{asset('userpanel/js/bootstrap.min.js')}}"></script>
<script src="{{asset('userpanel/js/jquery.bxslider.min.js')}}"></script>
<script src="{{asset('userpanel/js/jquery.countTo.js')}}"></script>
<script src="{{asset('userpanel/js/owl.carousel.min.js')}}"></script>
<script src="{{asset('userpanel/js/validation.js')}}"></script>
<script src="{{asset('userpanel/js/jquery.mixitup.min.js')}}"></script>
<script src="{{asset('userpanel/js/jquery.easing.min.js')}}"></script>
<script src="{{asset('userpanel/js/gmaps.js')}}"></script>
<script src="{{asset('userpanel/js/map-helper.js')}}"></script>
<script src="{{asset('userpanel/js/jquery.fancybox.pack.js')}}"></script>
<script src="{{asset('userpanel/js/jquery.appear.js')}}"></script>
<script src="{{asset('userpanel/js/isotope.js')}}"></script>
<script src="{{asset('userpanel/js/jquery.prettyPhoto.js')}}"></script> 
<script src="{{asset('userpanel/js/jquery.bootstrap-touchspin.js')}}"></script>
<script src="{{asset('userpanel/assets/bootstrap-sl-1.12.1/bootstrap-select.js')}}"></script>                               
<script src="{{asset('userpanel/assets/jquery-ui-1.11.4/jquery-ui.js')}}"></script>
<script src="{{asset('userpanel/assets/language-switcher/jquery.polyglot.language.switcher.js')}}"></script>
<script src="{{asset('userpanel/assets/html5lightbox/html5lightbox.js')}}"></script>
<script src="{{asset('userpanel/assets/revolution/js/jquery.themepunch.tools.min.js')}}"></script>
<script src="{{asset('userpanel/assets/revolution/js/jquery.themepunch.revolution.min.js')}}"></script>
<script src="{{asset('userpanel/assets/revolution/js/extensions/revolution.extension.actions.min.js')}}"></script>
<script src="{{asset('userpanel/assets/revolution/js/extensions/revolution.extension.carousel.min.js')}}"></script>
<script src="{{asset('userpanel/assets/revolution/js/extensions/revolution.extension.kenburn.min.js')}}"></script>
<script src="{{asset('userpanel/assets/revolution/js/extensions/revolution.extension.layeranimation.min.js')}}"></script>
<script src="{{asset('userpanel/assets/revolution/js/extensions/revolution.extension.migration.min.js')}}"></script>
<script src="{{asset('userpanel/assets/revolution/js/extensions/revolution.extension.navigation.min.js')}}"></script>
<script src="{{asset('userpanel/assets/revolution/js/extensions/revolution.extension.parallax.min.js')}}"></script>
<script src="{{asset('userpanel/assets/revolution/js/extensions/revolution.extension.slideanims.min.js')}}"></script>
<script src="{{asset('userpanel/assets/revolution/js/extensions/revolution.extension.video.min.js')}}"></script>



<script src="{{asset('userpanel/js/custom.js')}}"></script>
</body>
</html>