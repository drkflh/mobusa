@extends('userpanel.layouts.guest')

@section('title', "Form Pemesanan")

@section('content')
<section class="checkout-area">
    <div class="container">
        <div class="row">
            <div class="">
                <div class="form billing-info">
                    <div class="sec-title pdb-50">
                        <h1>Form Pembatalan</h1>
                        <span class="border"></span>
                    </div>
                    <form method="post" action="{{ route('search.pembatalan') }}">
                        @csrf
                        <div class="row">
                            <div class="col-md-5">
                                <div class="field-label">Nama *</div>
                                <div class="field-input">
                                    <input type="text" name="nama" placeholder="" required>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="field-label">No HP *</div>
                                <div class="field-input">
                                    <input type="text" name="no_hp" placeholder="" required>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="placeorder-button text-left">
                                    <button class="thm-btn" type="submit">Cari Data</button>
                                </div>   
                            </div>
                        </div>    
                    </form>
                </div>    
            </div>
        </div>
        
        @empty($data)
        <div class="row">
            <div class="col-md-12">
                <p>Data tidak ditemukan atau order tidak dalam status proses.</p>
            </div>
        </div>
        @else
        <div class="row bottom">
            <div class="col-lg-6 col-md-6">
                <div class="table">
                    <div class="sec-title">
                        <h1>Detail Pemesanan</h1>
                        <span class="border"></span>
                    </div>
                    <table class="cart-table">
                        <thead class="cart-header">
                            <tr>
                                <th class="product-column">Produk</th>
                                <th>Aroma</th>
                                <th>Jumlah</th>
                                <th class="price">Total</th>
                            </tr>    
                        </thead>
                        <tbody>
                            <tr>
                                <td class="product-column">
                                    <div class="column-box">
                                        <div class="product-title">
                                            <h3>{{ $data->product_pemesan }}</h3>
                                        </div>
                                    </div>
                                </td>
                                <td class="product-column">
                                    <div class="column-box">
                                        <div class="product-title">
                                            <h3>{{ $data->aroma }}</h3>
                                        </div>
                                    </div>
                                </td>
                                <td class="product-column">
                                    <div class="column-box">
                                        <div class="product-title">
                                            <h3>{{ $data->jumlah_product }}</h3>
                                        </div>
                                    </div>
                                </td>
                                <td class="product-column">
                                    <div class="column-box">
                                        <div class="product-title">
                                            <h3>Rp {{ number_format($data->total_harga, 0, ',', '.') }}</h3>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            
            <div class="col-lg-6 col-md-6">
                <div class="cart-total">
                    <div class="sec-title">
                        <h1>Detail COD</h1>
                        <span class="border"></span>
                    </div>
                    <ul class="cart-total-table">
                        <li class="clearfix">
                            <span class="col col-title">Lokasi COD</span>
                            <span class="col">{{ $data->lokasi_cod }}</span>    
                        </li>
                        <li class="clearfix">
                            <span class="col col-title">Tanggal COD</span>
                            <span class="col">{{ \Carbon\Carbon::parse($data->date)->translatedFormat('l, j F Y') }}</span>
                        </li>
                        <li class="clearfix">
                            <span class="col col-title">Waktu COD</span>
                            <span class="col">{{ $data->time }}</span>    
                        </li>      
                    </ul>
                    
                    <div class="payment-options">
                        <div class="placeorder-button text-left">
                            <form id="cancelForm" method="post" action="{{ route('order.cancel', $data->id) }}">
                                @csrf
                                <input type="hidden" name="alasan_cancel" id="cancel_reason">
                                <button class="thm-btn" id="cancelButton" type="button">Cancel Orderan?</button>
                            </form>
                        </div>
                    </div>             
                </div>    
            </div>
        </div>
        @endempty
    </div>
</section>  
<script>
    document.addEventListener('DOMContentLoaded', function () {
        const cancelButton = document.getElementById('cancelButton');
        cancelButton.addEventListener('click', function () {
            Swal.fire({
                title: 'Masukkan alasan pembatalan:',
                input: 'text',
                inputPlaceholder: 'Alasan pembatalan',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Lanjutkan',
                cancelButtonText: 'Batal'
            }).then((reason) => {
                if (reason.value) {
                    document.getElementById('cancel_reason').value = reason.value;
                    Swal.fire({
                        title: 'Benarkah ini Pesanan Anda dan ingin membatalkannya?',
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Ya, Batalkan Pesanan!',
                        cancelButtonText: 'Batal'
                    }).then((result) => {
                        if (result.isConfirmed) {
                            document.getElementById('cancelForm').submit();
                        }
                    });
                }
            });
        });
    });
</script>

@endsection
