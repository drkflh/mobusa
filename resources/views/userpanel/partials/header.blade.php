<section class="mainmenu-area stricky">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 clearfix">
                <div class="logo pull-left">
                    <a href="/">
                        <img src="{{asset('adminpanel/img/LogoMobusaPanjang.png')}}" alt="Awesome Logo">
                    </a>
                </div>
                <nav class="main-menu pull-left">
                    <div class="navbar-header">   	
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div class="navbar-collapse collapse clearfix">
                        <ul class="navigation clearfix">
                            <li><a href="/">Beranda</a></li>
                            <li><a href="#produk-kami" >Produk Kami</a></li>
                            <li><a href="/form-pemesanan">Form Pemesanan</a></li>
                            <li><a href="/form-pembatalan">Form Pembatalan</a></li>
                            <li><a href="#kontak">Kontak Kami</a></li>
                        </ul>
                    </div>
                </nav>
            </div>   
        </div>
    </div>
</section>

<script>
    document.addEventListener('DOMContentLoaded', function() {
        const links = document.querySelectorAll('a[href^="#"]');

        links.forEach(link => {
            link.addEventListener('click', function(event) {
                event.preventDefault();

                const targetId = this.getAttribute('href').substring(1);
                const targetElement = document.getElementById(targetId);

                if (targetElement) {
                    window.scrollTo({
                        top: targetElement.offsetTop,
                        behavior: 'smooth'
                    });
                }
            });
        });
    });
</script>
