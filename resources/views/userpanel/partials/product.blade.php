<section id="produk-kami" class="testimonial-area">
    <div class="container">
        <div class="sec-title text-center">
            <h1>Daftar Produk Kami</h1>
            <span class="border center"></span>
        </div> 
        <div class="row">
            <div class="col-md-6">
                <div class="single-testimonial-item">
                    <div class="img-holder">
                        <img src="{{asset('adminpanel/img/produk/1Botol.png')}}" alt="1Botol">
                    </div>
                    <div class="top">
                        <div class="left pull-left">
                            <h3>1 Botol</h3>
                        </div>
                    </div>
                    <div class="text-holder">
                        <p>Paket 1 Botol Ini Hanya Berisi Sabun Premium 1 Botol 250ml Dan Dapat Memilih Aromanya Yaitu Hydro, Akasia, Dan Philux Dengan Aroma Yang Sangat Wangi Pastinya.</p>
                    </div>
                    <div class="bottom">
                        <div class="left pull-left">
                            <h3>Rp. 17.000</h3>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="single-testimonial-item">
                    <div class="img-holder">
                        <img src="{{asset('adminpanel/img/produk/1Box.png')}}" alt="1Box">
                    </div>
                    <div class="top">
                        <div class="left pull-left">
                            <h3>1 Box</h3>
                        </div>
                        <div class="right pull-right">
                        </div>
                    </div>
                    <div class="text-holder">
                        <p>Paket 1 Box Terdiri Yang Berisikan 1 Bottle 250ml, ⁠1 Kain lap Microfiber 30x40, 1 Spons Tebal, Bonus Thankscard, Stikers. Dan Juga Dapat  Memilih Aromanya Yaitu Hydro, Akasia, Dan Philux.</p>
                    </div>
                    <div class="bottom">
                        <div class="left pull-left">
                            <h3>Rp. 25.000</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>