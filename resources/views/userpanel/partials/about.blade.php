<section id="tentangkami"class="welcome-area sec-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="text-holder">
                    <div class="sec-title">
                        <h1><span>Mobusa</span> Shampo Premium</h1>
                        <span class="border"></span>
                     </div> 
                    <div class="text-box">
                        <p>Mau hemat tapi pengin dapet kualitas yang maximal? solusinya treatment kendaraan dengan dengan Mobusa Shampo Premium🤩</p>
                        <p>Berikut Keuntungan Menggunakan Mobusa Shampo Premium : </p>
                        <p><strong>1. Safe To Use</strong></p>
                        <p>   Aman Digunakan Untuk Semua Jenis Kendaraan. Tidak Merusak Cat</p>
                        <p><strong>2. Extra Therefore</strong></p>
                        <p>   Busa Lebih Banyak Sehingga Membantu Menghilangkan Partikel Kotoran & Goresan Kecil</p>
                        <p><strong>3. Shampoo + Wax</strong></p>
                        <p>   Terdapat Kandungan WAX Pada Sabun MOBUSA Sehingga Membuat Cat Semakin Tajam & Mengkilap</p>
                        <p><strong>4. Affordable Price</strong></p>
                        <p>   Harga Lebih Terjangkau, Lebih Hemat Tanpa Menurunkan Kualitas</p>
                    </div>
                     
                </div>
            </div> 
            <div class="col-md-6">
                <div class="img-holder wow slideInRight text-center">
                    <img src="{{ asset('adminpanel/img/about.png') }}" alt="About Image Mobusa" width="200" height="200">
                </div>
            </div>   
        </div>
    </div>
</section>