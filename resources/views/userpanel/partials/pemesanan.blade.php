<section id="formpemesanan" class="appoinment-area">
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-sm-12 col-xs-12">
                <div class="appoinment">
                    <div class="sec-title">
                        <h1>Form Pemesanan</h1>
                        <span class="border"></span>
                    </div>
                    <form class="appoinment-form" id="formPemesanan" method="POST"
                        action="{{ route('datapemesanan.store') }}">
                        @csrf
                        <div class="row">
                            <div class="col-md-4">
                                <div class="input-label">
                                    <label>Nama *</label>
                                </div>
                                <div class="input-box">
                                    <input type="text" name="nama" placeholder="Nama Anda" required>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="input-label">
                                    <label>Nomor WA/HP *</label>
                                </div>
                                <div class="input-box">
                                    <input type="number" name="no_hp" placeholder="Nomor HP Anda" required>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="input-label">
                                    <label>Lokasi COD *</label>
                                </div>
                                <div class="input-box">
                                    <input type="text" name="lokasi_cod" placeholder="Lokasi COD" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="input-label">
                                    <label>Produk *</label>
                                </div>
                                <div class="input-box">
                                    <select class="selectmenu" name="product_pemesan" required>
                                        <option value="" disabled selected>Pilih Produk</option>
                                        <option value="1 BOX">Paket 1 Box</option>
                                        <option value="1 Botol">1 Botol 250ml</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="input-label">
                                    <label>Aroma *</label>
                                </div>
                                <div class="input-box">
                                    <select class="selectmenu" name="aroma" required>
                                        <option value="" disabled selected>Pilih Aroma</option>
                                        <option value="Hydro">Hydro</option>
                                        <option value="Akasia">Akasia</option>
                                        <option value="Philux">Philux</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="input-label">
                                    <label>Jumlah Produk *</label>
                                </div>
                                <div class="input-box">
                                    <input type="number" name="jumlah_product" placeholder="Jumlah Produk" min="1"
                                        required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="input-label">
                                    <label>Total Harga</label>
                                </div>
                                <div class="input-box">
                                    <input type="number" name="total_harga" id="total_harga" min="0" step="0.01"
                                        readonly>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="input-label">
                                    <label>Tanggal COD *</label>
                                </div>
                                <div class="input-box">
                                    <input type="date" name="date" placeholder="" required>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="input-label">
                                    <label>Jam COD *</label>
                                </div>
                                <div class="input-box">
                                    <input type="time" name="time" placeholder="" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-3 col-md-4">
                                <button class="thm-btn" id="btnPesan" type="button">Pesan Sekarang</button>
                            </div>
                            <div class="col-lg-9 col-md-12">
                                <div class="text">
                                    <p>* Team Kami Akan Mengkonfirmasi Melalui Whatsapp.
                                    </p>
                                    <p>* COD Dapat Dilakukan Jam 13.00 - 23.00 WIB.
                                    </p>
                                    <p>* COD Dapat Dilakukan Pada Hari Selasa, Jumat, Dan Sabtu.
                                    </p>
                                    <p>* COD Selain Hari Diatas Bisa Hubungi Team Kami <a href="https://wa.me/6281225667282">Click Disini!!</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-3">

            </div>
        </div>
        <div class="img-holder">
            <img src="{{asset('adminpanel/img/pemesanan.png')}}" alt="Awesome Image">
        </div>
    </div>
</section>

<script>
    document.addEventListener('DOMContentLoaded', function() {
    Swal.fire({
        title: 'Penting!!!',
        html: '<ul>' +
              '<li>1. COD Dilakukan Pada Hari Selasa, Jumat, Dan Sabtu.</li>' +
              '<li>2. COD Selain Hari Diatas Bisa Hubungi Team Kami.' +
              '<li>3. COD Dapat Dilakukan Jam 13.00 - 23.00 WIB.</li>' +
              '<li>4. Team Akan Mengkonfirmasi Melalui Whatsapp.</li>' +
              '</ul>',
        icon: 'info',
        confirmButtonText: 'Ok'
    });

    
    const timeInput = document.querySelector('input[name="time"]');

timeInput.addEventListener('input', function() {
    const selectedTime = this.value;
    const [hours, minutes] = selectedTime.split(':').map(Number);

    if (hours < 13 || hours >= 23) {
        Swal.fire({
            icon: 'error',
            title: 'Waktu tidak valid',
            text: 'Harap pilih waktu antara 13:00 dan 23:00.',
        });
        this.value = '';
    }
});

    const dateInput = document.querySelector('input[name="date"]');
        
        dateInput.addEventListener('input', function() {
            const selectedDate = new Date(dateInput.value);
            const day = selectedDate.getUTCDay();
            
            if (day !== 2 && day !== 5 && day !== 6) {
                Swal.fire({
                    icon: 'error',
                    title: 'Tanggal Tidak Valid',
                    text: 'COD hanya berlaku pada hari Selasa, Jumat, dan Sabtu.',
                });
                dateInput.value = ''; 
            }
        });

    const selectProduct = document.querySelector('select[name="product_pemesan"]');
    const jumlahProduct = document.querySelector('input[name="jumlah_product"]');
    const totalHarga = document.getElementById('total_harga');
    const formPemesanan = document.getElementById('formPemesanan');
    const btnPesan = document.getElementById('btnPesan');

    const hargaProduk = {
        '1 BOX': 25000,
        '1 Botol': 17000
    };

    function hitungTotalHarga() {
        const produk = selectProduct.value;
        const jumlah = jumlahProduct.value;
        if (produk in hargaProduk && jumlah > 0) {
            const hargaSatuan = hargaProduk[produk];
            const total = hargaSatuan * jumlah;
            totalHarga.value = total.toFixed(0);
        } else {
            totalHarga.value = '';
        }
    }

    selectProduct.addEventListener('change', hitungTotalHarga);
    jumlahProduct.addEventListener('input', hitungTotalHarga);

    btnPesan.addEventListener('click', function() {
        const nama = document.querySelector('input[name="nama"]').value.trim();
        const no_hp = document.querySelector('input[name="no_hp"]').value.trim();
        const lokasi_cod = document.querySelector('input[name="lokasi_cod"]').value.trim();
        const aroma = document.querySelector('select[name="aroma"]').value;
        const date = document.querySelector('input[name="date"]').value;
        const time = document.querySelector('input[name="time"]').value;

        if (nama === '' || no_hp === '' || lokasi_cod === '' || aroma === '' || date === '' || time === '') {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Harap lengkapi semua kolom sebelum memesan!!!',
            });
        } else {
            Swal.fire({
                title: 'Apakah Anda yakin?',
                text: "Anda akan memesan produk ini.",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, pesan sekarang!'
            }).then((result) => {
                if (result.isConfirmed) {
                    formPemesanan.submit();
                }
            });
        }
    });
});

</script>