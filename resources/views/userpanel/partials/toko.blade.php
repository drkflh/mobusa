<section class="model-area">
    <div class="container">
        <div class="sec-title text-center">
            <h1>Pemesanan Bisa Melalui</h1>
            <span class="border center"></span>
        </div>
        <div class="row text-center" style="justify-content: center;">
            <div class="col-md-4  col-sm-4 col-xs-12">
                <div class="single-model-item">
                    <a href="https://wa.me/6281225667282">
                        <img src="{{asset('adminpanel/img/toko/logo-whatsapp.png')}}" alt="Whatsapp">
                    </a>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="single-model-item">
                    <a href="https://www.instagram.com/mobusa.id">
                        <img src="{{asset('adminpanel/img/toko/logo-instagram.png')}}" alt="Instagram">
                    </a>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="single-model-item">
                    <a href="https://shopee.co.id/mobusa.id">
                        <img src="{{asset('adminpanel/img/toko/logo-shoppe.png')}}" alt="Shopee">
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>