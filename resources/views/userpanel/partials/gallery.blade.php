<section class="project-area">
    <div class="container-fluid">
        <div class="row mar0">
            <div class="single-project-item span-20per">
                <div class="img-holder">
                    <img src="{{asset('adminpanel/img/gallery/1.jpg')}}" alt="Product Gallery Mobusa">
                </div>   
            </div> 

            <div class="single-project-item span-20per">
                <div class="img-holder">
                    <img src="{{asset('adminpanel/img/gallery/2.jpg')}}" alt="Product Gallery Mobusa">
                </div>   
            </div> 

            <div class="single-project-item span-20per">
                <div class="img-holder">
                    <img src="{{asset('adminpanel/img/gallery/3.jpg')}}" alt="Product Gallery Mobusa">
                </div>   
            </div> 

            <div class="single-project-item span-20per">
                <div class="img-holder">
                    <img src="{{asset('adminpanel/img/gallery/4.jpg')}}" alt="Product Gallery Mobusa">
                </div>   
            </div> 
            <div class="single-project-item span-20per">
                <div class="img-holder">
                    <img src="{{asset('adminpanel/img/gallery/5.jpg')}}" alt="Product Gallery Mobusa">
                </div>   
            </div> 
        </div>
    </div>
</section> 
<section class="slogan-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="inner-content wow fadeInUp" data-wow-delay="200ms" data-wow-duration="1500ms">
                    <div class="title pull-left">
                        <h1>Auto Cleaning & Glowing Bersama MOBUSA Shampo Premium.</h1>
                    </div>
                    <div class="button pull-right">
                        <a class="thm-btn" href="/form-pemesanan">Pesan Sekarang!!!</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>  