<footer id="kontak" class="footer-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="single-footer-widget martop-minus pd-bottom50">
                    <div class="footer-logo">
                        <a href="index.html">
                            <img src="{{asset('adminpanel/img/LogoMobusa.png')}}" alt="Mobusa Logo Logo">
                        </a>
                    </div>
                    <div class="our-info">
                        <p>Mau hemat tapi pengin dapet kualitas yang maximal? solusinya treatment kendaraan dengan
                            dengan Mobusa Shampo Premium🤩
                        </p>
                    </div>

                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                <div class="single-footer-widget mar-bottom">
                    <div class="title">
                        <h3>Kontak Kami</h3>
                    </div>
                    <ul class="footer-contact-info">
                        <li>
                            <div class="icon-holder">
                                <span class="flaticon-maps-and-flags"></span>
                            </div>
                            <div class="text-holder">
                                <h5>Purwokerto, Banyumas</h5>
                            </div>
                        </li>
                        <li>
                            <div class="icon-holder">
                                <span class="flaticon-phone-receiver"></span>
                            </div>
                            <div class="text-holder">
                                <h5>+62 812-2566-7282</h5>
                            </div>
                        </li>
                        <li>
                            <div class="icon-holder">
                                <span class="flaticon-envelope"></span>
                            </div>
                            <div class="text-holder">
                                <h5>admin@mobusa.my.id</h5>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                <div class="single-footer-widget clearfix">
                    <div class="title">
                        <h3>Jam Pelayanan</h3>
                    </div>
                    <ul class="working-hours">
                        <li>Senin: <span>Online</span></li>
                        <li>Selasa: <span>Online</span></li>
                        <li>Rabu: <span>Online</span></li>
                        <li>Kamis: <span>Online</span></li>
                        <li>Jumat: <span>Online</span></li>
                        <li>Sabtu: <span>Online</span></li>
                        <li>Minggu: <span>Online</span></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>