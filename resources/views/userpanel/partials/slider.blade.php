<section id="beranda"class="rev_slider_wrapper">
    <div id="slider1" class="rev_slider"  data-version="5.0">
        <ul>
            <li data-transition="rs-20">
                <img src="{{asset('adminpanel/img/slider/banner1.jpg')}}"  alt="" width="1920" height="750" data-bgposition="top center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="1">
                
                
                <div class="tp-caption  tp-resizeme" 
                    data-x="right" data-hoffset="0" 
                    data-y="top" data-voffset="170" 
                    data-transform_idle="o:1;"         
                    data-transform_in="x:[-175%];y:0px;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0.01;s:3000;e:Power3.easeOut;" 
                    data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;" 
                    data-mask_in="x:[100%];y:0;s:inherit;e:inherit;" 
                    data-splitin="none" 
                    data-splitout="none"
                    data-responsive_offset="on"
                    data-start="1500">
                    <div class="slide-content-box mar-right text-center">
                        <h1>Mobusa Shampo Premium <br>Auto Cleaning & Glowing</h1>
                        <h2>Mau hemat tapi pengin dapet kualitas yang maximal? <br> solusinya treatment kendaraan dengan dengan Mobusa Shampo Premium🤩
                        </h2>
                    </div>
                </div>
               
                
            </li>
            <li data-transition="fade">
                <img src="{{asset('adminpanel/img/slider/banner2.jpg')}}" alt="" width="1920" height="750" data-bgposition="top center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="1" >
                
                <div class="tp-caption  tp-resizeme" 
                    data-x="left" data-hoffset="0" 
                    data-y="top" data-voffset="170" 
                    data-transform_idle="o:1;"         
                    data-transform_in="x:[-175%];y:0px;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0.01;s:3000;e:Power3.easeOut;" 
                    data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;" 
                    data-mask_in="x:[100%];y:0;s:inherit;e:inherit;" 
                    data-splitin="none" 
                    data-splitout="none"
                    data-responsive_offset="on"
                    data-start="1500">
                    <div class="slide-content-box mar-lft text-center">
                        <h1>Hemat Dan Bersih<br>Bersama Mobusa</h1>
                        <h2>Aman Digunakan Segala Jenis Cat, Busa Lebih Banyak,<br> Terdapat Kandungan WAX Sehingga Membuat Cat Semakin Tajam & Mengkilap,<br> Harga Lebih Terjangkau, Lebih Hemat Tanpa Menurunkan Kualitas</h2>
                    </div>
                </div>
                
                
            </li>
            <li data-transition="fade">
                <img src="{{asset('adminpanel/img/slider/banner3.jpg')}}"  alt="" width="1920" height="750" data-bgposition="top center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="1" >
                
                <div class="tp-caption  tp-resizeme" 
                    data-x="right" data-hoffset="0" 
                    data-y="top" data-voffset="170" 
                    data-transform_idle="o:1;"         
                    data-transform_in="x:[-175%];y:0px;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0.01;s:3000;e:Power3.easeOut;" 
                    data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;" 
                    data-mask_in="x:[100%];y:0;s:inherit;e:inherit;" 
                    data-splitin="none" 
                    data-splitout="none"
                    data-responsive_offset="on"
                    data-start="1500">
                    <div class="slide-content-box mar-right text-center">
                        <h1>Langsung Pesan<br>Sekarang!!!</span></h1>
                        <h2>Bisa Langsung Isi Form Dibawah <br>Atau Lewat Marketplace Kami Link Tersedia Dibawah</h2>
                    </div>
                </div>
                
            </li>
        </ul>
    </div>
</section>