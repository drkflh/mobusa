<?php

namespace App\Http\Controllers;

use App\Models\DataPemesanan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;


class DataPemesananController extends Controller
{
    public function index()
    {
        $dataPemesanans = DataPemesanan::orderBy('created_at', 'desc')
            ->get();
        return view('adminpanel.datapesanan', compact('dataPemesanans'));
    }


    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nama' => 'required|string|max:255',
            'no_hp' => 'required|string|max:20',
            'lokasi_cod' => 'required|string|max:255',
            'time' => 'required|string|max:50',
            'product_pemesan' => 'required|string|max:255',
            'aroma' => 'required|string|max:255',
            'jumlah_product' => 'required|integer|min:1',
            'total_harga' => 'required|numeric|min:0',
            'status_transaksi' => 'nullable|string|max:50',
            'alasan_cancel' => 'nullable|string|max:50',
            'date' => 'required|date',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $dataPemesanan = new DataPemesanan();
        $dataPemesanan->nama = $request->nama;
        $dataPemesanan->no_hp = $request->no_hp;
        $dataPemesanan->lokasi_cod = $request->lokasi_cod;
        $dataPemesanan->time = $request->time;
        $dataPemesanan->product_pemesan = $request->product_pemesan;
        $dataPemesanan->aroma = $request->aroma;
        $dataPemesanan->jumlah_product = $request->jumlah_product;
        $dataPemesanan->total_harga = $request->total_harga;
        $dataPemesanan->date = $request->date;
        $dataPemesanan->status_transaksi = $request->status_transaksi ?? 'proses';
        $dataPemesanan->alasan_cancel = $request->alasan_cancel ?? 'tidak ada';

        $dataPemesanan->save();

        return redirect()->back()->with('success', 'Data Berhasil Disimpan');

    }


    public function updateStatus(Request $request, $id)
    {
        $dataPemesanan = DataPemesanan::findOrFail($id);

        $dataPemesanan->status_transaksi = 'selesai';
        $dataPemesanan->save();

        return redirect()->back()->with('success', 'Status transaksi berhasil diubah.');
    }

    public function showSearchForm()
    {
        return view('userpanel.formpembatalan');
    }

    public function searchForCancellation(Request $request)
    {
        $request->validate([
            'nama' => 'required|string',
            'no_hp' => 'required|string',
        ]);

        $nama = $request->input('nama');
        $no_hp = $request->input('no_hp');

        $data = DataPemesanan::where('nama', $nama)
            ->where('no_hp', $no_hp)
            ->where('status_transaksi', 'proses')
            ->first();

        if (!$data) {
            return redirect()->back()->with('error', 'Order not found or not in process status.');
        }

        return view('userpanel.formpembatalan', compact('data'));
    }


    public function cancelOrder($id, Request $request)
    {
        $pemesanan = DataPemesanan::find($id);
    
        if ($pemesanan) {
            $pemesanan->status_transaksi = 'cancel';
            $pemesanan->alasan_cancel = $request->input('alasan_cancel');
            $pemesanan->save();
    
            return redirect()->back()->with('success', 'Order has been successfully cancelled.');
        }
    
        return redirect()->back()->with('error', 'Order not found.');
    }
    


}
