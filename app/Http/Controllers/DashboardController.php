<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\DataPemesanan;
use Carbon\Carbon;

class DashboardController extends Controller
{
    public function index()
    {
        $totalBarangSemua = DataPemesanan::where('status_transaksi', '!=', 'cancel')->sum('jumlah_product');

        $totalUangSemua = DataPemesanan::where('status_transaksi', '!=', 'cancel')->sum('total_harga');
    
        $totalBarangHariIni = DataPemesanan::where('status_transaksi', '!=', 'cancel')
                                            ->whereDate('date', Carbon::today())
                                            ->sum('jumlah_product');
    
        $totalUangHariIni = DataPemesanan::where('status_transaksi', '!=', 'cancel')
                                          ->whereDate('date', Carbon::today())
                                          ->sum('total_harga');
    
        $totalBarangPerMinggu = DataPemesanan::where('status_transaksi', '!=', 'cancel')
                                              ->whereBetween('date', [Carbon::now()->subDays(6), Carbon::now()])
                                              ->sum('jumlah_product');
    
        $totalUangPerMinggu = DataPemesanan::where('status_transaksi', '!=', 'cancel')
                                            ->whereBetween('date', [Carbon::now()->subDays(6), Carbon::now()])
                                            ->sum('total_harga');
    
        $totalBarangPerBulan = DataPemesanan::where('status_transaksi', '!=', 'cancel')
                                            ->whereBetween('date', [Carbon::now()->subDays(29), Carbon::now()])
                                            ->sum('jumlah_product');
    
        $totalUangPerBulan = DataPemesanan::where('status_transaksi', '!=', 'cancel')
                                          ->whereBetween('date', [Carbon::now()->subDays(29), Carbon::now()])
                                          ->sum('total_harga');
    
        $totalDiproses = DataPemesanan::where('status_transaksi', 'proses')->count();   
        $totalSelesai = DataPemesanan::where('status_transaksi', 'selesai')->count();
        $totalCancel = DataPemesanan::where('status_transaksi', 'cancel')->count();
    
        $dataPieChart = [
            'proses' => $totalDiproses,
            'selesai' => $totalSelesai,
            'cancel' => $totalCancel,
        ];
    
        $revenueByMonth = DataPemesanan::where('status_transaksi', '!=', 'cancel')
                                       ->selectRaw('SUM(total_harga) as total, YEAR(date) as year, MONTH(date) as month')
                                       ->groupBy('year', 'month')
                                       ->orderBy('year', 'desc')
                                       ->orderBy('month', 'desc')
                                       ->get();
    
        $revenueTrends = [];
        $currentMonthRevenue = null;
        foreach ($revenueByMonth as $month) {
            if (!$currentMonthRevenue) {
                $currentMonthRevenue = $month->total;
                $revenueTrends[] = null;
            } else {
                $previousMonthRevenue = $month->total;
                if ($currentMonthRevenue > $previousMonthRevenue) {
                    $revenueTrends[] = 'turun';
                } elseif ($currentMonthRevenue < $previousMonthRevenue) {
                    $revenueTrends[] = 'naik';
                } else {
                    $revenueTrends[] = 'sama';
                }
                $currentMonthRevenue = $previousMonthRevenue;
            }
        }
    
        return view('dashboard', compact(
            'totalBarangSemua', 'totalUangSemua', 
            'totalBarangHariIni', 'totalUangHariIni', 
            'totalBarangPerMinggu', 'totalUangPerMinggu', 
            'totalBarangPerBulan', 'totalUangPerBulan', 
            'totalDiproses', 'totalSelesai', 'totalCancel',
            'dataPieChart',
            'revenueByMonth', 'revenueTrends'
        ));
    }
    
}
