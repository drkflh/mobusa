<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DataPemesanan extends Model
{
    use HasFactory;
    protected $table = 'data_pemesanans';
    protected $fillable = [
        'nama',
        'no_hp',
        'lokasi_cod',
        'product_pemesan',
        'aroma',
        'jumlah_product',
        'total_harga',
        'time',
        'date',
        'status_transaksi',
        'alasan_cancel',
    ];
}
