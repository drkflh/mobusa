<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_pemesanans', function (Blueprint $table) {
            $table->id();
            $table->string('nama');
            $table->string('no_hp');
            $table->string('lokasi_cod');
            $table->string('product_pemesan');
            $table->string('aroma');
            $table->integer('jumlah_product');
            $table->decimal('total_harga', 10, 2);
            $table->time('time');
            $table->date('date');
            $table->string('status_transaksi')->default('proses');
            $table->string('alasan_cancel')->default('tidak ada');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_pemesanans');
    }
};
